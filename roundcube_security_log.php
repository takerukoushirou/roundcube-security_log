<?php
/*
 * RoundCube Security Log Plugin
 *
 * Copyright (C) 2021, Michael Maier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Security Log
 *
 * Logs authentication attempts to a configured sink, taking reverse proxy
 * headers into account to determine the actual client IP.
 *
 * Resulting logs can be processed locally or transferred out to the reverse
 * proxy to block brute force attacks. See README for examples.
 *
 * @author Michael Maier
 * @url https://gitlab.com/takerukoushirou/roundcube-security_log
 */
class roundcube_security_log extends rcube_plugin
{

    public $task = 'login';

    const DriverConfigKey = 'security_log_driver';

    const TrustedProxiesConfigKey = 'security_log_trusted_proxies';
    const ProxyAllowPrivateClientIpConfigKey = 'security_log_proxy_allow_private_client_ip';

    const FileConfigKey = 'security_log_file';

    const SyslogPrefixConfigKey = 'security_log_syslog_prefix';
    const SyslogPriorityConfigKey = 'security_log_syslog_priority';
    const SyslogFacilityConfigKey = 'security_log_syslog_facility';

    private $rc;

    public
    function init()
    {
        $this->rc = rcmail::get_instance();

        $this->load_config();

        $this->add_hook('login_failed', [ $this, 'on_login_failed' ]);
    }

    /**
     * Resolves the client IP by taking proxy headers into account.
     *
     * If REMOTE_ADDR matches one of the configured trusted proxies, common
     * proxy headers for forwarded client IPs are searched and the first valid
     * IP match is returned.
     *
     * By default, private range IP addresses are skipped unless explicitly
     * enabled in the configuration.
     *
     * @return string
     * Client IP address.
     */
    protected
    function get_client_ip()
    {
        $trusted_proxies = $this->rc->config->get(static::TrustedProxiesConfigKey);
        $allow_private_client_ip = $this->rc->config->get(static::ProxyAllowPrivateClientIpConfigKey);
        $remoteAddress = trim($_SERVER['REMOTE_ADDR']);

        if (is_array($trusted_proxies) and in_array($remoteAddress, $trusted_proxies)) {
            // Request originates from trusted proxy. Process common headers.
            $client_headers = [
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_CLUSTER_CLIENT_IP',
                'HTTP_FORWARDED_FOR',
                'HTTP_FORWARDED',
                'REMOTE_ADDR',
            ];
            $filter_flags = FILTER_FLAG_NO_RES_RANGE;

            if (!$allow_private_client_ip) {
                $filter_flags |= FILTER_FLAG_NO_PRIV_RANGE;
            }

            foreach ($client_headers as $header_key) {
                if (!empty ($_SERVER[$header_key])) {
                    $ips = explode(',', $_SERVER[$header_key]);

                    foreach ($ips as $ip) {
                        $ip = trim($ip);

                        if (false !== filter_var($ip, FILTER_VALIDATE_IP, $filter_flags)) {
                            return $ip;
                        }
                    }
                }
            }
        }

        if (empty($remoteAddress)) {
            return 'UNKNOWN';
        }

        return $remoteAddress;
    }

    /**
     * Simple escape for values reported in double quotes.
     *
     * @param string $message
     * String to escape.
     *
     * @return string
     * Escapes message.
     */
    protected
    function escape($message)
    {
        return strtr($message, '"', "'");
    }

    /**
     * Formats a comma-separated string representation of an array of data
     * fields.
     *
     * @param array $data
     * Data fields to format.
     *
     * @return string
     */
    protected
    function format_data_fields(array $data)
    {
        $data = array_map(
            function ($key, $value) {
                if (is_int($value) || is_float($value)) {
                    return "$key=$value";
                }
                else if ($value === null) {
                    return "$key=null";
                }
                else {
                    return sprintf(
                        '%s="%s"',
                        $key,
                        $this->escape((string) $value)
                    );
                }
            },
            array_keys($data),
            array_values($data)
        );

        return implode(', ', $data);
    }

    /**
     * Logs user name, host and error code on login failure.
     *
     * @param array $args
     */
    public
    function on_login_failed($args)
    {
        $this->report(
            'Login failed',
            [
                'host' => (string) $args['host'],
                'user' => (string) $args['user'],
                'code' => (int) $args['code'],
            ]
        );
    }

    /**
     * Logs a message to the configured sink. Client IP and timestamp are
     * logged automatically according to the configured sink.
     *
     * @param string $message
     * Message to log.
     *
     * @param array $data
     * Additional data fields to include in log. The data type for each field
     * should be carefully enforced to allow easier parsing for unstructured
     * sinks.
     *
     * @return bool
     * `true` on success, otherwise `false`.
     */
    protected
    function report($message, array $data)
    {
        $driver = strtolower($this->rc->config->get(static::DriverConfigKey));
        $client_ip = $this->get_client_ip();

        $writer_method = "report_to_$driver";

        if (!method_exists($this, $writer_method)) {
            rcube::write_log(
                static::class,
                'Configuration error: Driver not configured'
            );

            return false;
        }

        return $this->{$writer_method}($client_ip, $message, $data);
    }

    /** @noinspection PhpUnused */
    protected
    function report_to_file($client_ip, $message, array $data)
    {
        $log_file_name = $this->rc->config->get(static::FileConfigKey);

        $result = error_log(
            sprintf(
                "%s %s %s: %s\n",
                date('c'),
                $client_ip,
                $message,
                $this->format_data_fields($data)
            ),
            3,
            $log_file_name
        );

        if (!$result) {
            rcube::write_log(
                static::class,
                "Cannot write to file sink '$log_file_name': $php_errormsg"
            );

            return false;
        }

        return true;
    }

    /** @noinspection PhpUnused */
    protected
    function report_to_syslog($client_ip, $message, array $data)
    {
        $prefix = $this->rc->config->get(static::SyslogPrefixConfigKey);
        $priority = $this->rc->config->get(static::SyslogPriorityConfigKey);

        if (!strlen($prefix)) {
            $prefix = static::class;
        }

        if (static::is_os_windows()) {
            // Windows does not support different facilities.
            $facility = LOG_USER;
        }
        else {
            $facility = $this->rc->config->get(static::SyslogFacilityConfigKey);

            if (null === $facility) {
                // Default to LOG_AUTHPRIV, which may not be defined on every
                // system though. In that case fallback to LOG_AUTH.
                if (defined('LOG_AUTHPRIV')) {
                    $facility = constant('LOG_AUTHPRIV');
                }
                else {
                    $facility = LOG_AUTH;
                }
            }
        }

        if (null === $priority) {
            $priority = E_WARNING;
        }

        $result = openlog($prefix, LOG_NDELAY | LOG_PID, $facility);

        if (!$result) {
            rcube::write_log(
                static::class,
                "Failed to open syslog: $php_errormsg"
            );

            return false;
        }

        // Include client IP as data field.
        $data['client_ip'] = $client_ip;

        $result = syslog(
            $priority,
            "$message: " . $this->format_data_fields($data)
        );

        if (!$result) {
            rcube::write_log(
                static::class,
                "Failed to write to syslog: $php_errormsg"
            );

            return false;
        }

        closelog();

        return true;
    }

    /**
     * Determines if the current OS is Windows.
     *
     * @return bool
     * `true`, if OS is Windows, otherwise `false`.
     */
    static
    protected
    function is_os_windows()
    {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }

}